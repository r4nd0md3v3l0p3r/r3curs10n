﻿module R0.RecursionTests

open R0.Recursion
open Xunit
open FsUnit.Xunit

[<Fact>]
let ``FoldBackR test`` () = 
    let cons x xs =
        x::xs
    (FoldBackR (cons) [1..5] []) |> should equal [1..5]

[<Fact>]
let ``FoldBackRTC test`` () = 
    let consr xs x =
        x::xs
    (FoldBackRTC (consr) [1..5] []) |> should equal [1..5]

[<Fact>]
let ``FoldBackRC test`` () = 
    let cons x xs =
        x::xs
    (FoldBackRC (cons) [1..5] []) |> should equal [1..5]
