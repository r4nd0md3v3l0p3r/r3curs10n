﻿module R0.Recursion

(*
    Recursion w/o Tail Call:
    - Data for finishing computation for each element is stored on the stack frame before making the self call.
    - Once recursion bottoms out, stack starts unwrapping and thereby completing the computation of each element.
 *)
let rec FoldBackR combine l acc =
    match l with
    | h :: t -> combine h (FoldBackR combine t acc)
    | [] -> acc

(*
    FoldBackR (::) [1;2;3] []
    - 1 (::) (FoldBackR (::) [2;3] [])
      - 1 (::) (2 (::) (FoldBackR (::) [3] []))
        - 1 (::) (2 (::) (3 (::) (FoldBackR (::) [] []))
          - 1 (::) (2 (::) (3 (::) []))

    Note:
    - Data required for computation corresponding to each element is stored in the stack frames before the next self call is made
 *)

(*
    Recursion w/ Tail Call:
    - Data for finishing computation for each element is accumulated before making the self call.
    - Once the recusion bottoms out, the accumulated value is the result is which is then simply returned through all the stack frames
    - Tail call can be optimized into a while loop
 *)
let FoldBackRTC combine l acc =
    let rec Loop combine acc l =
        match l with
        | h :: t -> Loop combine (combine acc h) t
        | [] -> acc
    Loop combine acc (l |> List.rev)

(*
    FoldBackRTC (::) [1;2;3] []
    - List.rev [1;2;3] 
    - Loop (::) [] [3;2;1]
      - Loop (::) ((::) [] 3) [2;1]
        - Loop (::) ((::) [3] 2) [1]
          - Loop (::) ((::) [2;3] 1) []
            - [1;2;3]

    Note:
    - No data required for computation corresponding to elements is stored on the stack frames
    - ??? It appears that this is tail call optimized even when --tailcalls-
 *)


(*
    Continuations [w/ Tail calls]:
    For each element a lambda expression is generated representing the work needed to be done for that element.
    The value calculated within the lambda will then be passed to the lambda calculated for the previous element.
    Once we hit the end of the list, we essentially have a chain of lambda expressions which process each element in the list in reverse order. 
 *)
let FoldBackRC combine l acc =
    let rec Loop l cont =
        match l with
        | h :: t -> Loop t (fun racc -> cont (combine h racc))
        | [] -> cont acc
    Loop l (fun x -> x)

(*

    FoldBack (::) [1;2;3] []
    - Loop [1;2;3] (fun x -> x)
                    ^-----------
                                |
      - Loop [2;3] (fun racc -> cont (1 (::) racc))
                    ^-----------
                                |
        - Loop [3] (fun racc -> cont (2 (::) racc))
                    ^------------
                                 |
          - Loop [] (fun racc -> cont (3 (::) racc))
               ------^
              |
            - cont []
    Notes:
    - “racc” is the right-accumulated result of the tail-recursive call that will happen in the future
    - Each stack frame contains the following
      - Pointer to the closure: lambda:(fun racc -> cont (h (::) racc)), h & t 
    - Tail Recursion and Continuation are analogous
      - TR: you build the link; Continuation: link is continuation chain
      - Seed:               [] == (fun x -> x)
      - Step: Cons(data, next) == (fun racc -> cont (combine h racc)), h, cont
    - This is tail call optimized only on passing --tailcalls+
 *)
